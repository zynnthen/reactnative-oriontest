import {CONTACT_LIST, CONTACT_UPDATE} from '../actions/types';

const INITIAL_STATE = {
  contactList: [],
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case CONTACT_LIST:
      return {
        ...state,
        contactList: action.payload,
      };
    case CONTACT_UPDATE:
      return {
        ...state,
        contactList: state.contactList.map((contact) => {
          if (contact.id === action.payload.id) {
            return action.payload;
          }
          return contact;
        }),
      };
    default:
      return state;
  }
};
