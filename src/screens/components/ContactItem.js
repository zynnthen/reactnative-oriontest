import React, {Component} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {moderateScale} from 'react-native-size-matters';
import COLORS from '../styles/colors';
import styles from '../styles/styles';

class ContactItem extends Component {
  static defaultProps = {
    onPress: () => {},
  };

  onRowPress() {
    this.props.onPress(this.props.item);
  }

  render() {
    const {item} = this.props;

    return (
      <TouchableOpacity
        style={[styles.listItemContainer, styles.flexBetweenContainer]}
        onPress={this.onRowPress.bind(this)}>
        <View style={[styles.flexBetweenContainer]}>
          <View style={styles.circle} />
          <Text style={[styles.listPrimaryTextStyle, {marginLeft: 5}]}>
            {item.firstName} {item.lastName}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }
}

export default ContactItem;
