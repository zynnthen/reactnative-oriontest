import React, {Component} from 'react';
import {View, FlatList, Text, Button} from 'react-native';
import {connect} from 'react-redux';
import {getContacts} from '../../actions';
import ContactItem from '../components/ContactItem';
import COLORS from '../styles/colors';
import styles from '../styles/styles';

class ContactsScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
      contacts: [],
      error: null,
    };
  }

  static navigationOptions = {
    headerLeft: () => (
      <Button
        title="⌕"
        color={COLORS.THEME}
        style={styles.headerIcon}
        onPress={() => alert('Pressed search')}
      />
    ),
    headerRight: () => (
      <Button
        title="＋"
        color={COLORS.THEME}
        style={styles.headerIcon}
        onPress={() => alert('Pressed add')}
      />
    ),
  };

  componentDidMount() {
    this.props.getContacts((response, error) => {
      this.setState({error: error});
    });
  }

  componentDidUpdate(prevProps) {
    if (this.props.contactList !== prevProps.contactList) {
      this.setState({contacts: this.props.contactList});
    }
  }

  onItemPressed(item) {
    this.props.navigation.navigate('Detail', {selectedItem: item});
  }

  handleRefresh = () => {
    this.setState({refreshing: true}, () => {
      this.props.getContacts((response, error) => {
        this.setState({error: error});
      });
      this.setState({refreshing: false});
    });
  };

  render() {
    const {error, contacts} = this.state;

    return error != null ? (
      <View style={styles.centerContainer}>
        <Text style={styles.errorText}>{error}</Text>
      </View>
    ) : (
      <View style={styles.container}>
        <FlatList
          data={contacts}
          keyExtractor={(item) => item.id}
          extraData={this.state}
          refreshing={this.state.refreshing}
          onRefresh={this.handleRefresh}
          renderItem={({item}) => {
            return (
              <ContactItem
                item={item}
                onPress={(item) => this.onItemPressed(item)}
              />
            );
          }}
        />
      </View>
    );
  }
}

const mapStateToProps = ({contact}) => {
  const {contactList} = contact;
  return {contactList};
};

export default connect(mapStateToProps, {getContacts})(ContactsScreen);
