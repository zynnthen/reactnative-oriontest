import React, {Component} from 'react';
import {ScrollView, View, Text, Button, TextInput} from 'react-native';
import {connect} from 'react-redux';

import {updateContact} from '../../actions';

import COLORS from '../styles/colors';
import styles from '../styles/styles';

class DetailScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      item: {
        firstName: '',
        lastName: '',
        email: '',
        phone: '',
      },
    };
  }

  static navigationOptions = ({navigation}) => {
    const {state} = navigation;
    return {
      headerTitleStyle: {color: 'black'},
      headerTintColor: COLORS.THEME,
      headerBackTitle: 'Cancel',
      headerRight: () => (
        <Button
          onPress={() => {
            state.params.updateContact();
          }}
          title="Save"
          color={COLORS.THEME}
        />
      ),
    };
  };

  componentDidMount() {
    const {setParams} = this.props.navigation;
    setParams({updateContact: this.updateContact.bind(this)});
    this.setState({item: this.props.navigation.state.params.selectedItem});
  }

  updateContact() {
    if (this.state.item.firstName === '' || this.state.item.lastName === '') {
      alert('First and last name cannot be empty');
      return;
    }
    this.props.updateContact(this.state.item);
    alert('Saved');
  }

  render() {
    const {selectedItem} = this.props.navigation.state.params;
    const {item} = this.state;

    return (
      <ScrollView style={[styles.container, {marginTop: 10}]}>
        <View style={styles.centerContainer}>
          <View style={styles.circle} />
        </View>

        <Text style={styles.headerText}>Main Information</Text>
        <View style={styles.flexBetween}>
          <Text style={styles.label}>First Name</Text>
          <TextInput
            style={styles.textInput}
            onChangeText={(text) => {
              const item = Object.assign({}, this.state.item, {
                firstName: text,
              });
              this.setState({item});
            }}
            value={item.firstName}
            returnKeyType="next"
            onSubmitEditing={() => {
              this.lastNameTextInput.focus();
            }}
          />
        </View>
        <View style={styles.flexBetween}>
          <Text style={styles.label}>Last Name</Text>
          <TextInput
            style={styles.textInput}
            onChangeText={(text) => {
              const item = Object.assign({}, this.state.item, {
                lastName: text,
              });
              this.setState({item});
            }}
            value={item.lastName}
            ref={(input) => {
              this.lastNameTextInput = input;
            }}
            returnKeyType="next"
            onSubmitEditing={() => {
              this.emailTextInput.focus();
            }}
          />
        </View>

        <Text style={styles.headerText}>Sub Information</Text>
        <View style={styles.flexBetween}>
          <Text style={styles.label}>Email</Text>
          <TextInput
            style={styles.textInput}
            onChangeText={(text) => {
              const item = Object.assign({}, this.state.item, {
                email: text,
              });
              this.setState({item});
            }}
            value={item.email}
            ref={(input) => {
              this.emailTextInput = input;
            }}
            returnKeyType="next"
            onSubmitEditing={() => {
              this.phoneTextInput.focus();
            }}
          />
        </View>
        <View style={styles.flexBetween}>
          <Text style={styles.label}>Phone</Text>
          <TextInput
            style={styles.textInput}
            onChangeText={(text) => {
              const item = Object.assign({}, this.state.item, {
                phone: text,
              });
              this.setState({item});
            }}
            value={item.phone}
            ref={(input) => {
              this.phoneTextInput = input;
            }}
          />
        </View>
      </ScrollView>
    );
  }
}

export default connect(null, {updateContact})(DetailScreen);
