import {StyleSheet} from 'react-native';
import {scale, verticalScale, moderateScale} from 'react-native-size-matters';
import COLORS from './colors';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  centerContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  flexBetweenContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  flexBetween: {
    margin: scale(4),
    flexDirection: 'row',
    alignItems: 'center',
  },
  listItemContainer: {
    paddingTop: verticalScale(16),
    paddingLeft: scale(8),
    paddingRight: scale(8),
    paddingBottom: verticalScale(16),
    borderBottomColor: COLORS.GRAY_100,
    borderBottomWidth: 1,
  },
  label: {
    width: 100,
  },
  textInput: {
    marginLeft: scale(2),
    flex: 1,
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
  },
  headerText: {
    fontSize: moderateScale(20),
    fontWeight: 'bold',
    color: COLORS.TEXT_PRIMARY,
    padding: scale(4),
    backgroundColor: COLORS.GRAY_200,
  },
  headerIcon: {
    margin: scale(2),
  },
  subtitle: {
    fontSize: moderateScale(20),
    color: COLORS.TEXT_SECONDARY,
  },
  textMargin: {
    margin: scale(8),
  },
  listPrimaryTextStyle: {
    fontSize: moderateScale(20),
    color: COLORS.TEXT_PRIMARY,
  },
  listSecondaryTextStyle: {
    fontSize: moderateScale(14),
    color: COLORS.TEXT_SECONDARY,
  },
  errorText: {
    color: COLORS.TEXT_ERROR,
  },
  circle: {
    width: 50,
    height: 50,
    borderRadius: 50 / 2,
    backgroundColor: COLORS.THEME,
    margin: scale(2),
  },
});
