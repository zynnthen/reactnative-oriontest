import jsonData from '../../assets/data.json';
import {CONTACT_LIST, CONTACT_UPDATE} from './types';

export const getContacts = (callback) => async (dispatch) => {
  try {
    dispatch({type: CONTACT_LIST, payload: jsonData});
    callback(jsonData, null);
  } catch (error) {
    console.warn(error);
    callback(null, error);
  }
};

export const updateContact = (contact) => (dispatch) => {
  dispatch({type: CONTACT_UPDATE, payload: contact});
};
