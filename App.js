/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {Component} from 'react';
import {Provider} from 'react-redux';
import configureStore from './src/store';

import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';

import ContactsScreen from './src/screens/views/ContactsScreen';
import DetailScreen from './src/screens/views/DetailScreen';

const Container = createStackNavigator({
  Contacts: {
    screen: ContactsScreen,
  },
  Detail: {
    screen: DetailScreen
  }
});

const {store} = configureStore();
export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <AppContainer />
      </Provider>
    );
  }
}

const AppContainer = createAppContainer(Container);
